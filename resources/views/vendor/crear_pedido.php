<?php
/**
 * Created by PhpStorm.
 * User: alexisbarniquez
 * Date: 9/14/17
 * Time: 21:43
 */
?>
<!DOCTYPE html>
<html>
<head>
	<title>Pedidos</title>
	<link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

</head>
<body>
<div class="container-fluid">
	<div class="container">
		<div class="title">Bienvenido</div>
		<div class="row">
			<ul class="nav nav-pills nav-fill">
				<li class="nav-item">
					<a class="nav-link active" href="#">Inicio</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Crear Pedido</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Pedidos</a>
				</li>
				<li class="nav-item">
					<a class="nav-link disabled" href="#">Graficas</a>
				</li>
			</ul>
		</div>
	</div>
</body>
</html>
